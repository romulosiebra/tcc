var express = require('express');
var bodyParser = require('body-parser'); 
var trila = require('./Trilateration');

app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.post('/trilateration', (req, res) => {
    if(req.body){
        if(req.body.beacons.length < 3){
            return res.status(400).send({
                msg: 'Must be an array of 3 beacons with latidude, longitude, distance'
            })
        }
        var position = trila.trilaterate(req.body.beacons);
        console.log('Trilateração feita');
        return res.status(200).send({
            lat: position[0],
            lon: position[1]
        })
    }
    else{
        return res.status(400).send({
            msg: 'Need a body'
        })
    }
    

});

app.listen(8080,()=>{
    console.log('Servidor na porta 8080');
})